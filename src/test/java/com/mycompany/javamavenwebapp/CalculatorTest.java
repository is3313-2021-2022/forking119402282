/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javamavenwebapp;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author SWoodworth
 */
public class CalculatorTest {
    
    @Test
    public void testAdd() {
        int a = 5;
        int b = 7;
        Calculator instance = new Calculator();
        int expResult = 12;
        int result = instance.add(a, b);
        assertEquals(expResult, result);

    }
    
    @Test
    public void testSubtract() {
        int a = 5;
        int b = 7;
        Calculator instance = new Calculator();
        int expResult = -2;
        int result = instance.subtract(a, b);
        assertEquals(expResult, result);

    }
    
    @Test
    public void testMultiply() {
        int a = 5;
        int b = 7;
        Calculator instance = new Calculator();
        int expResult = 35;
        int result = instance.multiply(a, b);
        assertEquals(expResult, result);

    }
    
    @Test
    public void testDivide() {
        int a = 5;
        int b = 7;
        Calculator instance = new Calculator();
        int expResult = 0;
        int result = instance.divide(a, b);
        assertEquals(expResult, result);

    }
    
}
